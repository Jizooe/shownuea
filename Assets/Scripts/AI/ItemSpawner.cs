using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public GameObject itemPrefab;
    public float spawnInterval = 10f; // เวลาระหว่างการ spawn 
    private Transform aiTransform; // ตำแหน่งของ AI
    private float timeSinceLastSpawn = 0f;

    private void Start()
    {
        //ตำแหน่งTransform ของ AI โดยการหาวัตถุ AI ในเกม
        aiTransform = GameObject.Find("AI").transform;
    }
    
    private void Update()
    {
        timeSinceLastSpawn += Time.deltaTime;

        if (timeSinceLastSpawn >= spawnInterval)
        {
            SpawnItem();
            timeSinceLastSpawn = 0f;
        }
    }

    private void SpawnItem()
    {
        // สร้างรายการ (item) ในตำแหน่งที่คุณต้องการ และเพิ่มความเร็วของผู้เล่น
        GameObject newItem = Instantiate(itemPrefab, aiTransform.position, Quaternion.identity);
        
        //เวลาผ่านไปทำลาย
        Destroy(newItem, 5f); // 
    }
}