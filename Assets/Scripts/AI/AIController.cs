using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class AIController : MonoBehaviour
{
    public Transform[] wayPoint;
    private NavMeshAgent navMeshAgent;
    private int currentWayPoint = 0;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        // เริ่มการเดินจาก waypoint แรก
        SetRandomDestination();
    }

    // Update is called once per frame
    void Update()
    {
        // เช็คว่า AI ถึงแล้วหรือยัง
        if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance < 0.1f)
        {
            // เมื่อ AI เดินถึงจุดปลายทางให้สุ่มเลือก waypoint ต่อไป
            SetRandomDestination();
        }
    }

    // สุ่มเลือก waypoint และตั้งค่าให้ AI เดินไปยัง waypoint นั้น
    private void SetRandomDestination()
    {
        if (wayPoint.Length > 0)
        {
            int randomIndex = Random.Range(0, wayPoint.Length);
            navMeshAgent.SetDestination(wayPoint[randomIndex].position);
        }
    }
}