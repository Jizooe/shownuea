using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private GameObject MovingParticles;
    private Animator animator;
    
    private const string IS_RUN = "IsRunning";


    private void Awake() 
    {
        animator = GetComponent<Animator>();
        Hide();
    }
    
    
    private void Update() 
    {
        bool isRunning = player.IsRunning();
        animator.SetBool(IS_RUN, player.IsRunning());
        if (isRunning)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    private void Show()
    {
        MovingParticles.SetActive(true);
    }

    private void Hide()
    {
        MovingParticles.SetActive(false);
    }
}
