using System;
using System.Collections;
using System.Collections.Generic;
using Test;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour, IKitchenObjectParent
{
    public static Player Instance {get; private set;} 

    public event EventHandler<OnSelectedTableChangedEventArgs> OnSelectedTableChanged;
    public class OnSelectedTableChangedEventArgs : EventArgs 
    {
        public BaseTable selectedTable;
    }
    

    [SerializeField] private float moveSpeed = 7f;
    [SerializeField] private GameInput gameInput;
    [SerializeField] private LayerMask countersLayerMask;
    [SerializeField] private Transform kitchenObjectHoldPoint;
    
    [SerializeField] private LayerMask itemsLayerMask;
    private Item _item;
    [SerializeField] private LayerMask deadzoneLayerMask;
    private DeadZone _deadZone;

    private bool isRunning;

    private Vector3 lastInteractDir;
    private BaseTable selectedTable;
    private KitchenObject kitchenObject;
    
    //เสียง
    AudioManager audioManager;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("There is more than one Player instance");
        }
        Instance = this;
        
        audioManager = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioManager>();

    }

    private void Start()
    {
        gameInput.OnInteractAction += GameInput_OnInteractAction;
        gameInput.OnInteractCutAction += GameInput_OnInteractCutAction;
        
    }
    
    //interact ตัด
    private void GameInput_OnInteractCutAction(object sender, EventArgs e)
    {
        if (!KitchenGameManager.Instance.IsGamePlaying()) return;
        
        if (selectedTable != null)
        {
            selectedTable.InteractCut(this);
        }
    }
    
    //interact ปกติ
    private void GameInput_OnInteractAction(object sender, EventArgs e)
    {
        if (!KitchenGameManager.Instance.IsGamePlaying()) return;
        
        if (selectedTable != null)
        {
            selectedTable.Interact(this);
        }
    }

    private void Update()
    {
        HandleMovement();
        HandleInteractions();
        CheckForDeadZone();

    }

    private void HandleInteractions()
    {
        Vector2 inputVector = gameInput.GetMovementVectorNormalized();

        Vector3 moveDir = new Vector3(inputVector.x, 0f, inputVector.y);

        if (moveDir != Vector3.zero) //moveของplayer ไม่ใช่ 0 
        {
            lastInteractDir = moveDir; //ให้ค่า เป็น movedirและคำนวณ
        }

        float interactDistance = 2f; 
        //countersLayerMask = Layerของcounters
        if (Physics.Raycast(transform.position, lastInteractDir, out RaycastHit raycastHit, interactDistance, countersLayerMask))
        {
            // กระทำเมื่อมีการชนกับวัตถุบนเคาน์เตอร์
            if(raycastHit.transform.TryGetComponent(out BaseTable baseTable))
            {
                if (baseTable != selectedTable)
                {
                    SetSelectedTable(baseTable);
                }
            }else
            {
                SetSelectedTable(null);
            }
        }else 
        {
            SetSelectedTable(null); // กระทำเมื่อไม่มีการชนกับวัตถุบนเคาน์เตอร์
        }
        
        //itemsLayerMask
        if (Physics.Raycast(transform.position, lastInteractDir,out RaycastHit itemsraycastHit,interactDistance, itemsLayerMask))
        {
            if (itemsraycastHit.transform.TryGetComponent(out Item item))
            {
                if (item != _item)
                {
                    SetItem(item);
                    Debug.Log("ชนกับ item แล้ว");
                    //IncreaseSpeed(item.speedIncrease); // เพิ่มความเร็วของผู้เล่น
                    
                    //เล่นเสียง
                    audioManager.PlaySFX(audioManager.item);
                    StartCoroutine(IncreaseSpeedForDuration(item.speedIncrease, 3f)); // เพิ่มและลด

                    Destroy(item.gameObject); // ทำลาย GameObject ของไอเทม
                    
                    //Debug.DrawRay(transform.position, lastInteractDir * itemsraycastHit.distance, Color.red);  //โชว์ลำแสงการยิง
                }
            }
        }
    }
    
    
    private void CheckForDeadZone()
    {
        Vector3 moveDir = lastInteractDir; // เก็บค่า lastInteractDir ไว้ในตัวแปรเพื่อให้ตรวจสอบในทุกๆเฟรม

        // ปรับใช้ normalize ให้มีความยาวเท่ากับ 1
        moveDir.Normalize();

        float interactDistance = 4f;

        // ทำการรีเซ็ต _deadZone เป็น null เริ่มต้น
        SetDeadZone(null);

        // Cast a ray to check for collisions with dead zones
        if (Physics.Raycast(transform.position, moveDir, out RaycastHit deadZoneRaycastHit, interactDistance, deadzoneLayerMask))
        {
            if (deadZoneRaycastHit.transform.TryGetComponent(out DeadZone deadZone))
            {
                if (deadZone != _deadZone)
                {
                    SetDeadZone(deadZone);
                    
                    Debug.Log("ตุย");
                    
                     if (_deadZone != null)
                     {
                         _deadZone.TriggerRespawn();
                     }
                
                    //Debug.DrawRay(transform.position, moveDir * deadZoneRaycastHit.distance, Color.red); //ลำแสงการยิง
                }
            }
        }
    }
    
    
    
    private void HandleMovement()
    {
        Vector2 inputVector = gameInput.GetMovementVectorNormalized();

        Vector3 moveDir = new Vector3(inputVector.x, 0f, inputVector.y);

        float moveDistance = moveSpeed * Time.deltaTime;
        float playerRedius = 1f; //0.7
        float playerHeight = 3f; //2
        
        // ตำแหน่งและทิศทางของ CapsuleCast
        Vector3 capsuleCastStart = transform.position + Vector3.up * playerHeight;
        Vector3 capsuleCastEnd = capsuleCastStart + moveDir * moveDistance;
        
        bool canMove = !Physics.CapsuleCast(transform.position, transform.position + Vector3.up * playerHeight,
            playerRedius, moveDir, moveDistance);

        if (!canMove)
        {
            //Attemp only X movement
            Vector3 moveDirX = new Vector3(moveDir.x, 0, 0).normalized;
             canMove = moveDir.x !=0 && !Physics.CapsuleCast(transform.position, transform.position + Vector3.up * playerHeight,
                playerRedius, moveDirX, moveDistance);
            if (canMove)
            {
                moveDir = moveDirX;
            }
            else
            {
                //Attemp only Z movement
                Vector3 moveDirZ = new Vector3(0, 0, moveDir.z).normalized;
                 canMove = moveDir.z !=0 && !Physics.CapsuleCast(transform.position, transform.position + Vector3.up * playerHeight,
                    playerRedius, moveDirZ, moveDistance);

                if (canMove)
                {
                    moveDir = moveDirZ;
                }
                else
                {

                }
            }

        }
        if (canMove)
        {
            transform.position += moveDir * moveDistance;
        }
        
        isRunning = moveDir != Vector3.zero;
        float rotateSpeed = 10f;
        transform.forward = Vector3.Slerp(transform.forward, moveDir, Time.deltaTime * rotateSpeed);
    }

    private void SetSelectedTable(BaseTable selectedTable)
    {
        this.selectedTable = selectedTable;
        
        OnSelectedTableChanged?.Invoke(this, new OnSelectedTableChangedEventArgs
        {
            selectedTable = selectedTable
        });
    }
    
    public Transform GetKitchenObjectFollowTransform()
    {
        return kitchenObjectHoldPoint;
    }
    
    public void SetKitchenObject(KitchenObject kitchenObject)
    {
        this.kitchenObject = kitchenObject;
    }

    public KitchenObject GetKitchenObject()
    {
        return kitchenObject;
    }

    public void ClearKitchenObject()
    {
        kitchenObject = null;
    }

    public bool HasKitchenObject()
    {
        return kitchenObject != null;
    }

    public bool IsRunning() {
        return isRunning;
    }
    
    //เรียกใช้จากItem
    private void SetItem(Item item) 
    {
        this._item = item;
    }
    public void IncreaseSpeed(float speedIncrease)
    {
        moveSpeed += speedIncrease; // เพิ่มความเร็วของผู้เล่น
    }
    private IEnumerator IncreaseSpeedForDuration(float speedIncrease, float duration)
    {
        // เพิ่มความเร็ว
        IncreaseSpeed(speedIncrease);

        // รอเป็นเวลาที่ระบุ 
        yield return new WaitForSeconds(duration); //ใส่เวลาที่ต้องการ

        // คืนความเร็วเดิม
        IncreaseSpeed(-speedIncrease);
    }
    
    private void SetDeadZone(DeadZone deadZone) 
    {
        this._deadZone = deadZone;
    }
    
}


