using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ProgressBarUI : MonoBehaviour
{

    [SerializeField] private GameObject ProgressGameObject;
    [SerializeField] private Image barImage;

    private IProgress progress;

    private void Start()
    {
        progress = ProgressGameObject.GetComponent<IProgress>();
        if (progress == null)
        {
            Debug.Log("GameObject " + ProgressGameObject+ " don't have IProgress");
        }
        
        progress.OnProgressChanged += ProgressOn_ProgressChanged;
        
        barImage.fillAmount = 0f;

        Hide();
    }

    private void ProgressOn_ProgressChanged(object sender, IProgress.OnProgressChangedEventArgs e)
    {
        barImage.fillAmount = e.progressNormalized;

        if (e.progressNormalized == 0f || e.progressNormalized == 1f)
        {
            Hide();
        }
        else
        {
            Show();
        }
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }
    
    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
