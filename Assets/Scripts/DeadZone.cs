using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
    public Transform respawnPoint;


    public void TriggerRespawn()
    {
        // ย้ายผู้เล่นไปยังจุด respawnPoint
        Player.Instance.transform.position = respawnPoint.position;
        
    }
}
