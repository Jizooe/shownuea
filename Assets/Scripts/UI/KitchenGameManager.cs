using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenGameManager : MonoBehaviour
{
    public static KitchenGameManager Instance { get; private set; }
    
    public event EventHandler OnStateChanged;


    //เสียง
    AudioManager audioManager;
    private bool hasPlayedGameSummaryAudio = false;
    

    private enum State
    {
        LoadingToStart,
        CountdownToStart,
        GamePlaying,
        GameSummary,
    }

    private State _state;
    private float loadingToStartTimer = 1f;
    private float countdownToStartTimer = 3f; //นับถอยหลังก่อนที่จะให้เกมเริ่ม
    private float gamePlayingTimer;
    [SerializeField] private float gamePlayingTimerMax = 150f; //เวลาในการเล่น 
    
    
   // private float gameplayingToStartTimer = 30f; //เวลาในการเล่น

    private void Awake()
    {
        Instance = this;
        
        _state = State.LoadingToStart;
        audioManager = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioManager>();
    }
    
   
    private void Update()
    {
        switch (_state)
        {
            case State.LoadingToStart:
                loadingToStartTimer -= Time.deltaTime;
                if (loadingToStartTimer < 0f)
                {
                    _state = State.CountdownToStart;
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case State.CountdownToStart:
                countdownToStartTimer -= Time.deltaTime;
                if (countdownToStartTimer < 0f)
                {
                    _state = State.GamePlaying;
                    gamePlayingTimer = gamePlayingTimerMax;
                    
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case State.GamePlaying:
                gamePlayingTimer -= Time.deltaTime;
                if (gamePlayingTimer < 0f)
                {
                    _state = State.GameSummary;
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case State.GameSummary:
                if (!hasPlayedGameSummaryAudio) {
                    // เล่นเสียง
                    audioManager.PlaySFX(audioManager.gameend);
                    hasPlayedGameSummaryAudio = true;
                }
                break;
        }
        //Debug.Log(_state);
    }

    public bool IsGameStart()
    {
        return _state == State.CountdownToStart;
    }

    public bool IsGamePlaying() 
    {
        return _state == State.GamePlaying;
    }

    public bool IsGameSummary()
    {
        return _state == State.GameSummary;
    }
    
    public float GetGamePlayingTimerNormalized() {
        return 1 - (gamePlayingTimer / gamePlayingTimerMax);
    }
    
    
    public float GetGamePlayingTimer() //ใช้กับเวลาที่เป็นตัวเลข
    {
        return gamePlayingTimer; //เก็บเวลา
    }

    public float GetCountdownToStartTimer()
    {
        return countdownToStartTimer;
    }
    
}
