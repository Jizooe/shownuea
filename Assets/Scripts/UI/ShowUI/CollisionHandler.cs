using Test;
using UnityEngine;
using UnityEngine.UI;

public class CollisionHandler : MonoBehaviour, IActorEnterExitHandler, IInteractable
{
    [SerializeField] protected Image m_image;
   

    public void Interact(GameObject actor)
    {
       
    }
    
    
    public void ActorEnter(GameObject actor)
    {
        m_image.gameObject.SetActive(true);
    }

    public void ActorExit(GameObject actor)
    {
        m_image.gameObject.SetActive(false);
    }
}