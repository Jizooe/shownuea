using UnityEngine;

namespace Test
{
    public interface IInteractable
    {
        void Interact(GameObject actor);
    }
}