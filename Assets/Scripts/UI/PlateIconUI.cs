using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateIconUI : MonoBehaviour
{
    [SerializeField] private PlatesKitchenObject platesKitchenObject;
    [SerializeField] private Transform IconTemplate;  //ที่เก็บIcon

    private void Awake()
    {
        IconTemplate.gameObject.SetActive(false); //ซ่อนUI ตั้งแต่เริ่ม
    }

    private void Start()
    {
        platesKitchenObject.OnIngredientAdded += PlatesKitchenObject_OnIngredientAdded; //เหตุการณ์ของอาหารจากPlatesKitchenObject
    }

    private void PlatesKitchenObject_OnIngredientAdded(object sender, PlatesKitchenObject.OnIngredientAddedEventArgs e)
    {
        UpdateVisual();
    }

    private void UpdateVisual()
    {
        foreach (Transform child in transform) //เก็บค่าลูกของ transform แต่ล่ะรูปบนUI
        {
            if(child == IconTemplate) continue;
            Destroy(child.gameObject); //ทำลาย child ที่ไม่ใช่transform ถ้าไม่มีรูป Sprite ก็จะว่างป่าวเป็นสีขาว
        }
        
        foreach (KitchenObjectSO kitchenObjectSo in platesKitchenObject.GetKitchenObjectSOList())
        {
            Transform iconTransform = Instantiate(IconTemplate, transform); //ทำการคัดลอก UI
            iconTransform.gameObject.SetActive(true);
            iconTransform.GetComponent<PlateIconTemplateUI>().SetKitchenObjectSO(kitchenObjectSo); //เรียกใช้ PlateIconTemplateUI
        }
    }
}
