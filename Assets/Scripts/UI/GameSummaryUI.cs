using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting.Dependencies.NCalc;
using UnityEngine;

public class GameSummaryUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI SummaryScoreText;
    [SerializeField] private int maxScore; //ใส่คะแนนของด่านที่ต้องผ่าน เวลาโชว์หน้าUI
    

    private void Start()
    {
        KitchenGameManager.Instance.OnStateChanged += KitchenGameManager_OnStateChanged;

        Hide();
    }

    private void KitchenGameManager_OnStateChanged(object sender, System.EventArgs e)
    {
        if (KitchenGameManager.Instance.IsGameSummary())
        {
            Show();

           // SummaryScoreText.text = DeliveryManager.Instance.GetsummaryscoreAmout().ToString();
           int DeliveryScore = DeliveryManager.Instance.GetScore(); // รับคะแนนจาก DeliveryManager
           SummaryScoreText.text = DeliveryScore.ToString() + "             " + maxScore.ToString(); //โชว์คะแนนที่หน้าUI
           
        }
        else
        {
            Hide();
        }
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
    
}
