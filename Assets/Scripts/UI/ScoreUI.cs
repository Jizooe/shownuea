using UnityEngine;
using TMPro;

public class ScoreUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;

    private void Start()
    {
        KitchenGameManager.Instance.OnStateChanged += KitchenGameManager_OnStateChanged;
        
        Hide();
    }

    private void KitchenGameManager_OnStateChanged(object sender, System.EventArgs e)
    {
        if (KitchenGameManager.Instance.IsGamePlaying()) //state เกมplay
        {
            Update();
            Show();
        }
        else
        {
            Hide();
        }
    }
    
    private void Update()
    {
        int DeliveryScore = DeliveryManager.Instance.GetScore(); // รับคะแนนจาก DeliveryManager
        scoreText.text = DeliveryScore.ToString();
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
}