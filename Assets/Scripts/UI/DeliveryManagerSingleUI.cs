using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DeliveryManagerSingleUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI menuNameText;
    [SerializeField] private Transform iconContainer;
    [SerializeField] private Transform iconTemplate;
    
    [SerializeField] private Transform iconcookContainer;
    [SerializeField] private Transform iconcookTemplate;

    [SerializeField] private Transform iconmenuContainer;
    [SerializeField] private Transform iconmenuTemplate;

    private void Awake()
    {
        iconTemplate.gameObject.SetActive(false);
        iconcookTemplate.gameObject.SetActive(false);
        iconmenuTemplate.gameObject.SetActive(false);
    }

    //ตัวแปรเรียกชื่อmenu มาใส่ในtext
    public void SetMenuSO(MenuSO menuSO)
    {
        menuNameText.text = menuSO.menuName;

        foreach (Transform child in iconContainer)
        {
            if(child == iconTemplate) continue;
            Destroy(child.gameObject);
        }
        
        //โชว์รูปวัตถุดิบ
        foreach (KitchenObjectSO kitchenObjectSO in menuSO.kitchenObjectSOList)
        {
            Transform iconTransform = Instantiate(iconTemplate, iconContainer);
            iconTransform.gameObject.SetActive(true);
            iconTransform.GetComponent<Image>().sprite = kitchenObjectSO.sprite;
        }
        
        //โชว์รูปอุปกรณ์ทำอาหาร
        foreach (KitchenObjectSO kitchenObjectSO in menuSO.kitchenObjectSOList)
        {
            Transform iconTransform = Instantiate(iconcookTemplate, iconcookContainer);
            iconTransform.gameObject.SetActive(true);
            iconTransform.GetComponent<Image>().sprite = kitchenObjectSO.spritecook;
        }
        
        //โชว์รูปเมนู
        foreach (KitchenObjectSO kitchenObjectSO in menuSO.kitchenObjectSOList)
        {
            Transform iconTransform = Instantiate(iconmenuTemplate, iconmenuContainer);
            iconTransform.gameObject.SetActive(true);
            iconTransform.GetComponent<Image>().sprite = kitchenObjectSO.imageMenu;
        }
    }
}
