using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameStartUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI gameStartText;
    
    private void Start()
    {
        KitchenGameManager.Instance.OnStateChanged += KitchenGameManager_OnStateChanged;
        
        Hide();
    }

    private void KitchenGameManager_OnStateChanged(object sender, System.EventArgs e)
    {
        if (KitchenGameManager.Instance.IsGameStart())
        {
            ShowCountdown();
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if (KitchenGameManager.Instance.GetCountdownToStartTimer() > 0f)
        {
            float countdownTime = KitchenGameManager.Instance.GetCountdownToStartTimer();
            gameStartText.text = Mathf.Ceil(countdownTime).ToString();
        }
        else
        {
            gameStartText.text = "GO!";
        }
    }
    
    private void ShowCountdown()
    {
        gameObject.SetActive(true);
        StartCoroutine(CountdownAndHide());
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator CountdownAndHide()
    {
        gameStartText.text = "3";
        yield return new WaitForSeconds(1f);
        gameStartText.text = "2";
        yield return  new WaitForSeconds(1f);
        gameStartText.text = "1";
        yield return new WaitForSeconds(1f);
        Hide();
    }
}