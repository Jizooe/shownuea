using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

    public class GameAppFlowManager : MonoBehaviour
    {
        protected static bool IsSceneOptionsLoaded;
        
        [SerializeField] private ScoreSumarySO _scoreSumarySO;
        
        
        
        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }

        public void UnloadScene(string sceneName)
        {
            SceneManager.UnloadSceneAsync(sceneName);
        }

        public void LoadSceneAdditive(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }

        public void LoadOptionsScene(string optionSceneName)
        {
            if (!IsSceneOptionsLoaded)
            {
                SceneManager.LoadScene(optionSceneName, LoadSceneMode.Additive);
                IsSceneOptionsLoaded = true;
            }
        }

        public void UnloadOptionsScene(string optionSceneName)
        {
            if (IsSceneOptionsLoaded)
            {
                SceneManager.UnloadSceneAsync(optionSceneName);
                IsSceneOptionsLoaded = false;
            }
        }

        public void ExitGame()
        {
            Application.Quit();
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += SceneLoadedEventHandler;
            SceneManager.sceneUnloaded += SceneUnloadedEventHandler;
        }

        private void OnDisable()
        {
            SceneManager.sceneUnloaded -= SceneUnloadedEventHandler;
            SceneManager.sceneLoaded -= SceneLoadedEventHandler;
        }

        private void SceneUnloadedEventHandler(Scene scene)
        {

        }

        private void SceneLoadedEventHandler(Scene scene, LoadSceneMode mode)
        {
            //If the loaded scene is not the SceneOptions, set flag IsOptionsLoaded to false
            if (scene.name.CompareTo("SceneOptions") != 0)
            {
                IsSceneOptionsLoaded = false;
            }
        }
        
        public void PassLevel(string nextSceneName)
        {
            // ตรวจสอบคะแนนของผู้เล่นจากคลาส DeliveryManager (แก้ตามคลาสที่เก็บคะแนนของคุณ)
            int playerScore = DeliveryManager.Instance.GetScore();

            // ตรวจสอบคะแนนที่จำเป็นในการผ่านด่านต่อไป
            if (playerScore >= _scoreSumarySO.scoreSumary)
            {
                // โหลดซีนต่อไป
                LoadScene(nextSceneName);
            }
            else
            {
                // แสดงข้อความหรือทำอย่างอื่นที่คุณต้องการในกรณีที่ผู้เล่นไม่ผ่านด่านต่อไป
                Debug.Log("คะแนนไม่เพียงพอในการผ่านด่านต่อไป");
            }
        }
        
       

        
    }

