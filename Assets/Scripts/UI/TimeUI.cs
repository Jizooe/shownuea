using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimeUI : MonoBehaviour
{
    
    [SerializeField] private Image timerImage;
    [SerializeField] private TextMeshProUGUI timerText;
    public Gradient fillColor;
    
    private void Start()
    {
        KitchenGameManager.Instance.OnStateChanged += KitchenGameManager_OnStateChanged;

        Hide();
    }
    

    private void KitchenGameManager_OnStateChanged(object sender, System.EventArgs e)
    {
        if (KitchenGameManager.Instance.IsGamePlaying()) //state เกมplay
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    private void Update()
    {
        timerImage.fillAmount = KitchenGameManager.Instance.GetGamePlayingTimerNormalized();
        timerImage.color = fillColor.Evaluate(timerImage.fillAmount);
        float timeRemaining = Mathf.Clamp(KitchenGameManager.Instance.GetGamePlayingTimer(), 0f, float.MaxValue);
            
        // แสดงเวลาถอยเป็นตัวเลข
        int minutes = Mathf.FloorToInt(timeRemaining / 60);
        int seconds = Mathf.FloorToInt(timeRemaining % 60); 
        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
