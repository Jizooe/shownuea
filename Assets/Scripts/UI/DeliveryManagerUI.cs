using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DeliveryManagerUI : MonoBehaviour
{
   [SerializeField] private Transform container;
   [SerializeField] private Transform menuTemplate;
   
   //ทดสอบ
   [SerializeField] private Image menuTimerImage; 
   


   
   
   private void Awake()
   {
      // ตั้งค่าของแม่แบบ (template) เป็น false ที่นี่
      menuTemplate.gameObject.SetActive(false);
   }


   private void Start()
   {
      DeliveryManager.Instance.OnMenuSpawned += DeliveryManager_OnMenuSpawned;
      DeliveryManager.Instance.OnMenuCompleted += DeliveryManager_OnMenuCompleted;
      
      KitchenGameManager.Instance.OnStateChanged += KitchenGameManager_OnStateChanged;

      UpdateVisual();
   }

   private void DeliveryManager_OnMenuCompleted(object sender, System.EventArgs e)
   {
      UpdateVisual();

   }

   private void DeliveryManager_OnMenuSpawned(object sender, System.EventArgs e)
   {
      UpdateVisual();

   }

   private void UpdateVisual()
   {
      //menuTimerImage.fillAmount = DeliveryManager.Instance.GetMenuTimer(); //ทดลองทำเวลาของเมนู
      
      // ลบ UI เก่าออกจาก container
      foreach (Transform child in container)
      {
         if (child == menuTemplate) continue;
         Destroy(child.gameObject);
      }

      // สร้างแสดงผล UI เมนูใหม่
      foreach (MenuSO menuSO in DeliveryManager.Instance.GetWaitingMenuSOList())
      {
         Transform menuTransform = Instantiate(menuTemplate, container);
         menuTransform.gameObject.SetActive(true);
         menuTransform.GetComponent<DeliveryManagerSingleUI>().SetMenuSO(menuSO); //เรียกใช้จาก DeliveryManagerSingleUI
         
      }
      
      
   }
   
   private void KitchenGameManager_OnStateChanged(object sender, System.EventArgs e)
   {
      if (KitchenGameManager.Instance.IsGameSummary())
      {
         Hide();
      }
   }
   
   private void Hide()
   {
      gameObject.SetActive(false);
   }

   /*private void Update() 
   {
      menuTimerImage.fillAmount = DeliveryManager.Instance.GetMenuTimer();
   }*/
}
