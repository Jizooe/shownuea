using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_2 : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 7f;
    
    private float originalSpeed;
    
    private void Start()
    {
        originalSpeed = moveSpeed;
    }
    
    private void Update()
    {
        Vector2 inputVector = new Vector2(0, 0);
        
        if (Input.GetKey(KeyCode.UpArrow))
        {
            inputVector.y = +1;
        }
        
        if (Input.GetKey(KeyCode.DownArrow))
        {
            inputVector.y = -1;
        }
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            inputVector.x = -1;
        }
        
        if (Input.GetKey(KeyCode.RightArrow))
        {
            inputVector.x = +1;
        }

        inputVector = inputVector.normalized;

        Vector3 moveDir = new Vector3(inputVector.x, 0f, inputVector.y);
        transform.position += moveDir * moveSpeed * Time.deltaTime;
        
       // Debug.Log(Time.deltaTime);
    }

    public void IncreaseSpeed(float speed)
    {
        moveSpeed += speed;
        StartCoroutine(ResetSpeedAfterDelay(speed, 3.0f)); // เรียก Coroutine
    }

    private IEnumerator ResetSpeedAfterDelay(float speedBoostAmount, float delay)
    {
        yield return new WaitForSeconds(delay);

        // หลังจาก 3 วินาที, กลับความเร็วเป็นค่าเดิม
        moveSpeed = originalSpeed;
    }
    
    
}
