using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInput : MonoBehaviour
{
    public static GameInput Instance { get; private set; }
    
    public event EventHandler OnInteractAction; 
    public event EventHandler OnInteractCutAction;

    private PlayerInputAction playerInputAction;

    private void Awake()
    {
        Instance = this;
        
        playerInputAction = new PlayerInputAction(); //เรียกใช้ playerInputAction
        playerInputAction.Player.Enable(); //เปิดการทำงาน
        
        //เรียใช้ inputaction ของ Interact
        playerInputAction.Player.Interact.performed += Interact_performed; //Interact E
        playerInputAction.Player.InteractCut.performed += InteractCut_performed; //ตัด F

    }
    
    
    private void InteractCut_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        OnInteractCutAction?.Invoke(this, EventArgs.Empty);
    }
    
    private void Interact_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        OnInteractAction?.Invoke(this, EventArgs.Empty);
    }
    public Vector2 GetMovementVectorNormalized()
    {
        Vector2 inputVector = playerInputAction.Player.Movement.ReadValue<Vector2>();
        
        inputVector = inputVector.normalized;

        return inputVector;
    }
}


