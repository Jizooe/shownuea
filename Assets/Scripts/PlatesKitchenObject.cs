using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatesKitchenObject : KitchenObject
{
    //EventของPlateCompleteVisual
    public event EventHandler <OnIngredientAddedEventArgs> OnIngredientAdded;

    public class OnIngredientAddedEventArgs : EventArgs
    {
        public KitchenObjectSO kitchenObjectSO;
    }
    

    [SerializeField] private List<KitchenObjectSO> validKitchenObjectSOList; //ListของSOวัตถุดิบที่กำหนดให้สามารถเก็บได้

    private List<KitchenObjectSO> kitchenObjectSOList; //ListของSOวัตถุดิบที่หลังจากเก็บเข้ามา

    private void Awake()
    {
        kitchenObjectSOList = new List<KitchenObjectSO>();
    }

    public bool TryAddIngredient(KitchenObjectSO kitchenObjectSO)
    {
        //ถ้าไม่อยุ่ในListที่กำหนดให้เก็บ
        if (!validKitchenObjectSOList.Contains(kitchenObjectSO))
        {
            //ไม่ใช่สิ่งที่ให้เก็บ 
            return false;
        }
        
        //ถ้าเก็บSOเข้าListแล้ว
        if (kitchenObjectSOList.Contains(kitchenObjectSO))
        {
            //มีอยู่ในList
            return false;
        }
        else
        {
            //ถ้าไม่มีทั้ง 2 ให้เก็บ
            kitchenObjectSOList.Add(kitchenObjectSO);
            
            OnIngredientAdded?.Invoke(this, new OnIngredientAddedEventArgs
            {
                kitchenObjectSO = kitchenObjectSO
            });
            return true;
        }
    }

    public List<KitchenObjectSO> GetKitchenObjectSOList()
    {
        return kitchenObjectSOList;
    }
}
