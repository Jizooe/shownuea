using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class DeliveryManager : MonoBehaviour
{
    public event EventHandler OnMenuSpawned;
    public event EventHandler OnMenuCompleted;
    public event EventHandler OnMenuSuccess;
    public event EventHandler OnMenuFailed;

    public static DeliveryManager Instance { get; private set; }

    [SerializeField] private MenuListSO menuListSO; //เรียกใช้ MenuListSO

    private List<MenuSO> waitingmenuSOList;
    private float spawnMenuTimer;
    private float spawnMenuMax = 3f; //เวลาในการสปอน 3
    private int waitingMenuMax = 2; //จำนวนเมนูที่จะให้แสดงสูงสุด

    private int summaryscoreAmout;
    private int score;
    
    //เวลาในเมนู
    private float menuTimer;
    private float menuTimerMax = 30; //เวลาในการทำอาหารในรายการเมนู
    
    //เสียง
    AudioManager audioManager;
    
    
    private void Awake()
    {
        Instance = this;
        waitingmenuSOList = new List<MenuSO>();
        audioManager = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioManager>();

    }

    private void Update()
    {
        spawnMenuTimer -= Time.deltaTime;
         if (spawnMenuTimer <= 0f)
         {
             spawnMenuTimer = spawnMenuMax;
             
             if (KitchenGameManager.Instance.IsGamePlaying() && waitingmenuSOList.Count < waitingMenuMax) //ถ้าจำนวนเมนูในlistน้อยกว่าจำนวนเมนูที่กำหนด
             {

                 MenuSO waitingMenuSO = menuListSO.menuSOList[Random.Range(0, menuListSO.menuSOList.Count)];
                 
                 menuTimer = waitingMenuSO.cookingTime; // ตั้งค่าเวลาที่เหลือในการทำอาหารตามเมนู

                 Debug.Log(waitingMenuSO.menuName);
                 Debug.Log(waitingMenuSO.cookingTime);
                 

                 waitingmenuSOList.Add(waitingMenuSO);
                 
                 OnMenuSpawned?.Invoke(this, EventArgs.Empty);
                 
             }
         }

        
         menuTimer -= Time.deltaTime;
         //Debug.Log(menuTimer);
         if (menuTimer <= 0)
             {
                 for (int i = 0; i < waitingmenuSOList.Count; i++)
                 {
                     // เมนูทำไม่ทันเวลา
                     Debug.Log("หมดเวลาสนุกแล้วสิๆ");
                     waitingmenuSOList.RemoveAt(i);
                 }
             }
    }

    public void DeliveryMenu(PlatesKitchenObject platesKitchenObject)
    {
        for (int i = 0; i < waitingmenuSOList.Count; i++)
        {
            MenuSO waitingmenuSO = waitingmenuSOList[i];

            if (waitingmenuSO.kitchenObjectSOList.Count == platesKitchenObject.GetKitchenObjectSOList().Count)
            {
                //มีจำนวนส่วนผสมเท่ากัน
                bool plateContentsMatchesMenu = true;
                foreach (KitchenObjectSO menuKitchenObjectSO in waitingmenuSO.kitchenObjectSOList)
                {
                    //หมุนเวียนดูส่วนผสมทั้งหมดในเมนู
                    bool ingredientFound = false;
                    foreach (KitchenObjectSO plateKitchenObjectSO in platesKitchenObject.GetKitchenObjectSOList())
                    {
                        //ไล่ดูส่วนผสมทั้งหมดในจาน
                        if (plateKitchenObjectSO == menuKitchenObjectSO)
                        {
                            //ส่วนผสมเข้ากัน!
                            ingredientFound = true;
                            break;
                        }
                    }

                    if (!ingredientFound)
                    {
                        //ไม่เจอส่วนผสมของเมนูนี้ในจาน
                        plateContentsMatchesMenu = false;

                    }
                }

                if (plateContentsMatchesMenu)
                {
                    summaryscoreAmout++;

                    //ทำถูกต้อง
                    Debug.Log("เสริฟได้");
                    waitingmenuSOList.RemoveAt(i);

                    // เพิ่มคะแนนของสูตรอาหารเมื่อส่งสำเร็จ
                    int menuScore = waitingmenuSO.menuScore;
                    score += menuScore;
                    Debug.Log(menuScore);
                        
                    //เล่นเสียง
                    audioManager.PlaySFX(audioManager.menuSuccess);
                        
                    OnMenuCompleted?.Invoke(this, EventArgs.Empty);
                    OnMenuSuccess?.Invoke(this, EventArgs.Empty);
                        
                    return;
                    
                }
            }
        }
            
        //เล่นเสียง
        audioManager.PlaySFX(audioManager.menuFailed);
        
        OnMenuFailed?.Invoke(this, EventArgs.Empty);
        //ทำไม่ตรงเมนู
        Debug.Log("ทำผิดแล้ว");
    }


    public int GetsummaryscoreAmout()
    {
        return summaryscoreAmout;
    }

    public int GetScore() //เก็บค่าscore 
    {
        return score;
    }

    public List<MenuSO> GetWaitingMenuSOList()
    {
        return waitingmenuSOList;
    }
    
    public float GetMenuTimer()
    {
        return menuTimer;
    }


}
 