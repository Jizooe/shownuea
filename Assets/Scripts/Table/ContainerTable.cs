using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerTable : BaseTable
{
    [SerializeField] private KitchenObjectSO kitchenObjectSo;

    public event EventHandler OnPlayerGrabbedObject; 

    public override void Interact(Player player)
    {
        if (!player.HasKitchenObject()) //playerถือของอยู่มั้ย?
        {
            //ไม่ได้ถือ
            KitchenObject.SpawnKitchenObject(kitchenObjectSo, player); //เรียกkitchenObjectSoขึ้นมา

            OnPlayerGrabbedObject?.Invoke(this, EventArgs.Empty); //EventArgsที่ยกของถือ
        }
    }

}
