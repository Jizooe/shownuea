using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTable : MonoBehaviour, IKitchenObjectParent
{
    [SerializeField] private Transform counterTopPointPerfeb;

    private KitchenObject kitchenObject;

     
    public virtual void Interact(Player player)
    {
        Debug.LogError("BaseCounter.Interact()");
    }
    
    public virtual void InteractCut(Player player) 
    {
        //Debug.LogError("BaseCounter.InteractCut()");
    }
    
    public Transform GetKitchenObjectFollowTransform()
    {
        return counterTopPointPerfeb; //ตำแหน่งของที่วาง ที่กำหนดไว้ เป็น transform
    }
    
    public void SetKitchenObject(KitchenObject kitchenObject)
    {
        this.kitchenObject = kitchenObject;
    }

    public KitchenObject GetKitchenObject()
    {
        return kitchenObject;
    }

    public void ClearKitchenObject()
    {
        kitchenObject = null;
    }

    
    public bool HasKitchenObject()
    {
        return kitchenObject != null; //!=null คือมีของวางบนโต๊ะ และ null คือไม่มี ในตอนนี้
    }

}
