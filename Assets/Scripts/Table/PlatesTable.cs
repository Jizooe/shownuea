using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatesTable : BaseTable
{
    public event EventHandler OnPlateSpawned;
    public event EventHandler OnPlateRemoved;
    
    [SerializeField] private KitchenObjectSO plateKitchenObjectSO; //กำหนดชื่อตัวแปร
 
    private float spawnPlateTimer;  //เวลาในการสปอน
    private float spawnPlateTimerMax = 5f; //เวลาในการสปอนสูงสุด
    private int platesSpawnedAmount; //จาน
    private int platesSpawnedAmountMax = 2;  //จำนวนจานสูงสุด

    private void Update()
    {
        //อ้างอิงจาก Time.deltaTime
        spawnPlateTimer += Time.deltaTime;
        if (spawnPlateTimer > spawnPlateTimerMax)
        {
            spawnPlateTimer = 0f;

            if (platesSpawnedAmount < platesSpawnedAmountMax) 
            {
                platesSpawnedAmount++; 
                
                OnPlateSpawned?.Invoke(this, EventArgs.Empty); //eventแจ้งการเกิดของจาน เพื่อที่จะรีเซ็ตเวลาเตรียมสร้างอันต่อๆไป
            }
        }
    }

    public override void Interact(Player player)
    {
        //Playerไม่ได้ถืออะไร
        if (!player.HasKitchenObject()) 
        {
            if (platesSpawnedAmount > 0) //เช็คจำนวนจานว่ามีอยู่ไหม
            {
                platesSpawnedAmount--;

                KitchenObject.SpawnKitchenObject(plateKitchenObjectSO, player); //เปลี่ยนให้จานมาอยู่ในมือของplayer ตามSOที่สร้าง
                
                OnPlateRemoved?.Invoke(this, EventArgs.Empty); //eventแจ้งจานหายไปอยู่ในมือplayerแทนแล้ว
            }
        }
    }
}
