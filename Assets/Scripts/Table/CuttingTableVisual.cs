using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingTableVisual : MonoBehaviour
{
    private const string CUT = "Cut";

    [SerializeField] private CuttingTable _cuttingTable;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        _cuttingTable.OnCut += CuttingTable_OnCut;
    }

    private void CuttingTable_OnCut(object sender, System.EventArgs e)
    {
        _animator.SetTrigger(CUT);
    }
}
