using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatesTableVisual : MonoBehaviour
{
    [SerializeField] private PlatesTable platesTable;
    [SerializeField] private Transform conterTopPoint;
    [SerializeField] private Transform plateVisualPrefab;

    private List<GameObject> plateVisualGameObjectList; //เก็บListของGameObjectที่แสดงบนโต๊ะ

    [SerializeField] private Vector3 plateSize = new Vector3(1f, 1f, 1f);
    
    private void Awake()
    {
        plateVisualGameObjectList = new List<GameObject>();
    }

    private void Start()
    {
        //ใช้เชื่อมกันกับ eventของ PlatesTable
        platesTable.OnPlateSpawned += PlatesTable_OnPlateSpawned; //สร้างจาน
        platesTable.OnPlateRemoved += PlatesTable_OnPlateRemoved; //ลบจาน
    }

    
    private void PlatesTable_OnPlateRemoved(object sender, System.EventArgs e)
    {
        GameObject plateGameObject = plateVisualGameObjectList[plateVisualGameObjectList.Count - 1];
        plateVisualGameObjectList.Remove(plateGameObject);
        Destroy(plateGameObject);
    }
    
    private void PlatesTable_OnPlateSpawned(object sender, System.EventArgs e)
    {
        Transform plateVisualTransform = Instantiate(plateVisualPrefab, conterTopPoint); //สร้างจานใหม่

        // กำหนดขนาดของจานตามค่า plateSize
        plateVisualTransform.localScale = plateSize;
        
        float plateOffsetY = .1f;
        plateVisualTransform.localPosition = new Vector3(0, plateOffsetY * plateVisualGameObjectList.Count, 0);
        
        plateVisualGameObjectList.Add(plateVisualTransform.gameObject);
    }
}
 