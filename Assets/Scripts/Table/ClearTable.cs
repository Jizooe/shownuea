using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearTable : BaseTable
{

   // [SerializeField] private KitchenObjectSO KitchenObjectSo;
    
    public override void Interact(Player player)
    {
        //โต๊ะไม่มีวัตถุ
        if (!HasKitchenObject())
        {
            //playerถือของ
            if (player.HasKitchenObject())
            {
                //Playถือของอยู่
                player.GetKitchenObject().SetKitchenOjectParent(this); //เปลี่ยนให้ของที่Playerถือมาให้โต๊ะแทน
            }
            else
            {
                //Player not carrying anything
            }
        }
        else
        {
            //โต๊ะมีวัตถุ
            if (player.HasKitchenObject())
            {
                //ถ้ามีPlatesKitchenObjectอยู่ในมือ
                if (player.GetKitchenObject().TryGetPlate(out PlatesKitchenObject platesKitchenObject))
                {
                    //เพิ่มวัตถุดิบเข้าได้
                    if (platesKitchenObject.TryAddIngredient(GetKitchenObject().GetKitchenObjectSo()))
                    {
                        GetKitchenObject().DestroySelf();  
                    }
                }
                else //ไม่ได้ถือ
                {
                    //ถ้าบนโต๊ะมีจาน
                    if (GetKitchenObject().TryGetPlate(out platesKitchenObject))
                    {
                        if(platesKitchenObject.TryAddIngredient(player.GetKitchenObject().GetKitchenObjectSo()))
                        {
                            player.GetKitchenObject().DestroySelf();
                        }
                    }
                } 
            }
            else
            {
                //Playerไม่ได้ถือ
                GetKitchenObject().SetKitchenOjectParent(player); //เปลี่ยนให้ของที่อยู่บนโต๊ะมาให้playerถือ
            }
        }
    }
}
