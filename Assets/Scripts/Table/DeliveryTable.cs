using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryTable : BaseTable
{
    public override void Interact(Player player)
    {
        if (player.HasKitchenObject())
        {
            if (player.GetKitchenObject().TryGetPlate(out PlatesKitchenObject platesKitchenObject))
            {
                //เฉพาะจาน
                DeliveryManager.Instance.DeliveryMenu(platesKitchenObject);
                player.GetKitchenObject().DestroySelf();
            }
        }
    }
    
}
