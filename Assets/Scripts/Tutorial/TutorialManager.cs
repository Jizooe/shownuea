using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] private GameObject stepTitle;
    [SerializeField] private TutorialDescription description;
    [SerializeField, Min(0f)] private float timeBetweenSteps = 0.4f;
    [SerializeField] private GameObject[] arrows;

    private int currentStep = 0; // เริ่มขั้นตอนที่ 0

    private void Start()
    {
        // เริ่มการทำงานของ Tutorial ที่นี่
        StartTutorial();
        description.UnComplete();
    }

    private void StartTutorial()
    {
        // เริ่มที่ขั้นตอนที่ 0
        if (currentStep == 0)
        {
            // แสดงหัวเรื่องของขั้นตอน
            stepTitle.SetActive(true);

            // ตั้งค่าข้อความใน TutorialDescription
            description.SetUncompletedText("Use W A S D for player.");

            // แสดงลูกศรหรือการนำทางไปยังขั้นตอนถัดไป
            ShowArrow(0);

            // เริ่มต้นการตรวจสอบหากผู้เล่นทำขั้นตอนนี้เสร็จสิ้นแล้ว
            StartCoroutine(CheckForCompletion());
        }
    }

    private void ShowArrow(int arrowIndex)
    {
        for (int i = 0; i < arrows.Length; i++)
        {
            arrows[i].SetActive(i == arrowIndex);
        }
    }

    private IEnumerator CheckForCompletion()
    {
        yield return new WaitForSeconds(timeBetweenSteps);

        // ตรวจสอบว่าผู้เล่นได้ทำขั้นตอนนี้เสร็จสิ้นแล้วหรือยัง
        // และเรียกขั้นตอนถัดไปหากเสร็จสิ้นแล้ว โดยใช้ currentStep++
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {

            // ซ่อน TutorialDescription
            description.Complete();

            // ซ่อนลูกศร
            ShowArrow(-1);

            // เริ่มขั้นตอนถัดไป
            StartTutorial();

            currentStep++;
        }
        else
        {
            // รอตรวจสอบอีกครั้งถ้ายังไม่เสร็จสิ้น
            StartCoroutine(CheckForCompletion());
        }
    }
}
