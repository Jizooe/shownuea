using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class TutorialManager1 : MonoBehaviour
{
    public Text tutorialText;
    public GameObject nextStepArrow;
    public GameObject arrowPrefab; // Arrow prefab
    public Transform arrowParent; // Parent transform for arrows

    public int currentStep = 0;

    public string[] tutorialSteps =
    {
        "Step 1: เคลื่อนที่ด้วย W A S D",
        "Step 2: ดูเมนูอาหารจากขวาบน",
        "Step 3: ตัดผัก",
        "Step 4: หยิบผักจากในกล่อง",
        "Step 5: วางที่เขียงและกด F สับ",
        "Step 6: ทอดหมู",
        "Step 7: หยิบหมูจากกในตู้",
        "Step 8: วางที่เขียงและกด F หั่น",
        "Step 9: นำไปทอดที่กระทะกด E ทอด",
        "Step 10: หยิบน้ำซอสจากบนหม้อ",
        "Step 11: วางไว้ที่โต๊ะ",
        "Step 12: หยิบถ้วยไปใส่ซอส",
        "Step 13: หยิบผักกับหมูที่ทำเสร็จแล้วใส่ถ้วย",
        "Step 14: เสริฟอาหารจบ",
        // Add more steps as needed
    };

    private List<int> arrowCounts = new List<int>
    {
        0, 0, 0, 1, 2, 0, 1, 1, 1, 1, 1, 2, 2, 1 // ตัวอย่างจำนวนลูกศรในแต่ละขั้นตอน
    };

    public GameObject[] arrowPositions; // An array of GameObjects representing arrow positions
    private List<GameObject> arrows = new List<GameObject>();

    private void Start()
    {
        // Initialize the tutorial with the first step
        UpdateTutorialText();
        ShowArrowsForCurrentStep();
    }

    private void Update()
    {
        // Check for player input to proceed to the next step
        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentStep++;
            if (currentStep < tutorialSteps.Length)
            {
                UpdateTutorialText();
                ShowArrowsForCurrentStep();
            }
            else
            {
                // Tutorial completed, hide the tutorial UI
                tutorialText.text = "Tutorial Completed!";
                nextStepArrow.SetActive(false);
                // Add code to move to the main game or perform other actions
            }
        }
    }

    private void UpdateTutorialText()
    {
        // Update the tutorial text with the current step
        tutorialText.text = tutorialSteps[currentStep];
    }

    private void ShowArrowsForCurrentStep()
    {
        // Hide or destroy previous arrows
        foreach (var arrow in arrows)
        {
            Destroy(arrow);
        }

        // Create arrows for the current step
        arrows.Clear();
        if (currentStep < arrowCounts.Count)
        {
            int arrowCount = arrowCounts[currentStep];
            int startIndex = 0;

            // Check if it's step 5 and you want to start from a different element
            if (currentStep == 6)
            {
                startIndex = 2; // Set your desired start index
            }
            
            if (currentStep == 7)
            {
                startIndex = 11; 
            }
            
            if (currentStep == 8)
            {
                startIndex = 3; 
            }
            
            if (currentStep == 9)
            {
                startIndex = 4; 
            }
            
            if (currentStep == 10)
            {
                startIndex = 5; 
            }
            
            if (currentStep == 11)
            {
                startIndex = 5; 
            }
            
            if (currentStep == 12)
            {
                startIndex = 8; 
            }
            
            if (currentStep == 13)
            {
                startIndex = 10; 
            }

            for (int i = startIndex; i < arrowPositions.Length; i++)
            {
                if (arrowCount > 0)
                {
                    GameObject position = arrowPositions[i];
                    // Create arrow and set the position
                    GameObject newArrow = Instantiate(arrowPrefab, position.transform.position, Quaternion.identity);
                    newArrow.transform.SetParent(arrowParent);
                    arrows.Add(newArrow);
                    arrowCount--;
                }
                else
                {
                    break; // Break the loop when all arrows are created
                }
            }
        }
    }
    
}
