using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public sealed class TutorialDescription : MonoBehaviour
{
    [SerializeField] private TMP_Text text;
    [SerializeField] private CanvasGroup group;
    [SerializeField, Min(0f)] private float fadeSpeed = 1.2f;

    
    
    [Header("Colors")] 
    [SerializeField] private Color uncompletedColor;
    [SerializeField] private Color completedColor = Color.green;
    [SerializeField] private Color noncompletedColor = Color.white;
    
    private void Enable() => group.alpha = 1F;
    private void Disable() => group.alpha = 0F;
    
    private void Reset()
    {
        group = GetComponent<CanvasGroup>();
        text = GetComponentInChildren<TMP_Text>();

        uncompletedColor = text.color;
    }

    public void SetUncompletedText(string value) 
    {
        text.text = value;
        text.color = uncompletedColor;
        Enable();
    }

    public void UnComplete()
    {
        text.color = noncompletedColor;
    }
    
    public void Complete()
    {
        text.color = completedColor;
    }

    public IEnumerator FadeOutRoutine()
    {
        Enable();

        do
        {
            yield return null;
            group.alpha -= fadeSpeed * Time.deltaTime;

        } while (group.alpha > 0);

        Disable();
    }
    
    public Color GetTextColor()
    {
        return text.color;
    }
    
}