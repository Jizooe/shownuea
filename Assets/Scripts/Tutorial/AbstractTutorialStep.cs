using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractTutorialStep : MonoBehaviour
{
    protected TutorialManager manager;
    internal abstract string GetDescription();

}