using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IKitchenObjectParent
{
    public Transform GetKitchenObjectFollowTransform(); //ส่งคืน Transformที่ใช้ในการติดตามวัตถุในครัว(KitchenObject)
    public void SetKitchenObject(KitchenObject kitchenObject); //กำหนดวัตถุในครัว(KitchenObject)ที่ถูกวางบนโต๊ะ
    public KitchenObject GetKitchenObject(); //ส่งคืน KitchenObject ที่วางบนโต๊ะ
    public void ClearKitchenObject(); //ล้าง KitchenObject ที่วางบนโต๊ะ
    public bool HasKitchenObject(); //เช็ค object อยู่บนโต๊ะ?
    
}
