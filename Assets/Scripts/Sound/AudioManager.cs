using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [Header("Audio Source")] 
    [SerializeField] private AudioSource musicSource;
    [SerializeField] private AudioSource SFXSource;
    [SerializeField] private AudioSource buttonSource;    

    [Header("Audio Clip")] 
    public AudioClip cutting;  
    public AudioClip cooker; 
    public AudioClip menuSuccess; 
    public AudioClip menuFailed; 
    public AudioClip gameend;
    public AudioClip item;


    public static AudioManager instance;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
    }

    private void Start()
    {
        musicSource.Play();
    }

    public void PlaySFX(AudioClip clip)
    {
        SFXSource.PlayOneShot(clip);
    }
    
    public void PlayButtonSound(AudioClip clip)
    {
        buttonSource.PlayOneShot(clip);
    }
    
}
