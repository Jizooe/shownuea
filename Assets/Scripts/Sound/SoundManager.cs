using System;
using UnityEngine;
using UnityEngine.UI;

    public class SoundManager : MonoBehaviour
    {
        [SerializeField] protected SoundSettings m_soundSettings;

        public Slider m_SliderMasterVolume;
        public Slider m_SliderMusicVolume;
        public Slider m_SliderSFXVolume;
        public Slider m_SliderUIVolume;

        void Start()
        {
            InitialiseVolumes();
        }

        private void InitialiseVolumes()
        {
            SetMasterVolume(m_soundSettings.Master);
            SetMusicVolume(m_soundSettings.Music);
            SetSFXVolume(m_soundSettings.SFX);
            SetUIVolume(m_soundSettings.UI);
        }

        public void SetMasterVolume(float vol)
        {
            m_soundSettings.AudioMixer.SetFloat(m_soundSettings.MasterName, vol);
            m_soundSettings.Master = vol;
            m_SliderMasterVolume.value = m_soundSettings.Master;
        }

        public void SetMusicVolume(float vol)
        {
            m_soundSettings.AudioMixer.SetFloat(m_soundSettings.MusicName, vol);
            m_soundSettings.Music = vol;
            m_SliderMusicVolume.value = m_soundSettings.Music;
        }
        
        public void SetSFXVolume(float vol)
        {
            m_soundSettings.AudioMixer.SetFloat(m_soundSettings.SFXName, vol);
            m_soundSettings.SFX = vol;
            m_SliderSFXVolume.value = m_soundSettings.SFX;
        }

        public void SetUIVolume(float vol)
        {
            m_soundSettings.AudioMixer.SetFloat(m_soundSettings.UIName, vol);
            m_soundSettings.UI = vol;
            m_SliderUIVolume.value = m_soundSettings.UI;
        }
    }
