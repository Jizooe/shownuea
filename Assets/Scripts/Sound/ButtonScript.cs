using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    public AudioClip buttonSound;

    private AudioSource audioSource;  // สร้างตัวแปร AudioSource

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();  // เรียก AudioSource จาก GameObject ปุ่ม
    }

    public void OnButtonClicked()
    {
        if (audioSource != null && buttonSound != null)
        {
            audioSource.PlayOneShot(buttonSound);  // เล่นเสียงปุ่มพร้อมกับเสียงอื่นๆ
        }
    }
}