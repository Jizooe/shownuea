using UnityEngine;
using UnityEngine.Audio;


    [CreateAssetMenu(menuName = "Sound&Music", fileName = "SoundSettingsPreset")]
    public class SoundSettings : ScriptableObject
    {
        public AudioMixer AudioMixer;

        [Header("MasterVolume")] public string MasterName = "Master";
        [Range(-80, 20)] public float Master;

        [Header("MusicVolume")] public string MusicName = "Music";
        [Range(-80, 20)] public float Music;

        [Header("SFXVolume")] public string SFXName = "SFX";
        [Range(-80, 20)] public float SFX;

        [Header("UIVolume")] public string UIName = "UI";
        [Range(-80, 20)] public float UI;
    }