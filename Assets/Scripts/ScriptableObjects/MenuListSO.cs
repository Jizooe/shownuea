using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class MenuListSO : ScriptableObject
{
    public List<MenuSO> menuSOList; //เก็บ MenuSO เข้ามาไว้ที่นี่
}
 