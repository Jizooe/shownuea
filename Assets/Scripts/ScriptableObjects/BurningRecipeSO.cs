using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class BurningRecipeSO : ScriptableObject
{
    public KitchenObjectSO input; //โชว์ก่อนตัด
    public KitchenObjectSO output; // แสดงผลลัพธ์ หลังไหม้
    public float burningTimeMax; //เวลาการไหม้

}
