using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class MenuSO : ScriptableObject
{
   public List<KitchenObjectSO> kitchenObjectSOList; //เก็บSOของ kitchenObject รวมส่วนผสมของเมนูไว้ที่นี่ และตั้งชื่อเมมเบอร์ของKใหม่
   public string menuName; //ชื่อของเมนู
   public int menuScore; //คะแนนของอาหา
   public float cookingTime; // เวลาในการทำอาหาร

}
