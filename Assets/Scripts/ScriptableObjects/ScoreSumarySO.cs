using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ScoreSumarySO : ScriptableObject
{
    public int scoreSumary; //กำหนดคะแนนที่ต้องผ่านด่าน
}
