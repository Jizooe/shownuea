using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class FryingRecipeSO : ScriptableObject
{
    public KitchenObjectSO input; //โชว์ก่อนตัด
    public KitchenObjectSO output; // แสดงผลลัพธ์ หลังตัด
    public float fryingTimeMax; //เวลาการทอด

}
